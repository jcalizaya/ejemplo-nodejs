# Taller Tech Hub 

Ejemplo para taller	(NodeJS)

## Requisitos 

Debe tener instalado lo siguiente:

> NodeJS


## Descargar el proyecto

```sh
$ git clone <url del proyecto>
```

## Ejecutar los archivos

Si se está utilizando nvm, asegurarse que se está usando la versión de node correcta.

```sh
$ nvm use 8.9.4
```

Para ejecutar los archivos `.js` utilice node, ejemplo:

```sh
$ node 1-uno.js
```

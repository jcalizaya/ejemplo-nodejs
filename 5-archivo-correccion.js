var fs = require('fs');

var buffer = new Buffer(100000);
var archivo;

fs.open('ejemplo1.txt', 'r', (err, handle) => {

    if (err) {
        console.log("\n***No se encontró el archivo\n");
        return;
    }
    archivo = handle;
    console.log("archivo cargado.... ");
    console.log("archivo: ", archivo);

    fs.read(archivo, buffer, 0, 100000, null, (err, length) => {

        if (err) {
            console.log("\n***No se pudo leer el archivo\n");
            return;
        }

        console.log(buffer.toString('utf8', 0, length));
        fs.close(handle, () => { });
    });

});



